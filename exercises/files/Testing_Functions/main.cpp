// You do not need to modify this file.
#include "Functions.h"

int main()
{
    // Run tests
    Test_GetArea();
    Test_GetStringLength();
    Test_GetPerimeter();
    Test_Withdraw();

    return 0;
}
