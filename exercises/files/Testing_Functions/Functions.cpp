#include "Functions.h"

#include <iostream>
#include <string>
using namespace std;

// - PART 1: Stub functions ---------------------------------
// These functions just have placeholder values to satisfy the return type of their functions.
// These placeholders are meant to be replaced once tests are in place.

//! Returns the area of a rectangle given the `width` and `length`
int GetArea( int width, int length )
{
    return -1; // temporary return
}

//! Unit tests for the GetArea function
void Test_GetArea()
{
}

//! Returns the amount of characters in the `text` string
int GetStringLength( string text )
{
    return -1; // temporary return
}

//! Unit tests for the GetStringLength function
void Test_GetStringLength()
{
}

// - PART 2: Broken functions ------------------------------
// These functions are "implemented" but have logic errors. They should be flagged as
// failing on your unit tests. After you get the failing tests, you can come back
// and fix the logic.

//! Calculates and returns the perimeter of a rectangle given the `width` and the `length`
int GetPerimeter( int width, int length )
{
    return width + length; // Incorrect logic
}

//! Unit tests for the GetPerimeter function
void Test_GetPerimeter()
{
}

//! Returns the updated balance after `amount` is taken out from the original `balance`
float Withdraw( float balance, float amount )
{
    return balance; // Incorrect logic
}

//! Unit tests for the Withdraw function
void Test_Withdraw()
{
}
